"use strict";

if (!String.prototype.trim) {
    String.prototype.trim = function() {
        return this.replace(/^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g, "");
    };
}

if (!Function.prototype.bind) {
    Function.prototype.bind = function(oThis) {
        if (typeof this !== 'function') {
            throw new TypeError('Function.prototype.bind - what is trying to be bound is not callable');
        }

        let aArgs = Array.prototype.slice.call(arguments, 1),
            fToBind = this,
            fNOP    = function() {},
            fBound  = function() {
                return fToBind.apply(this instanceof fNOP && oThis
                    ? this
                    : oThis,
                    aArgs.concat(Array.prototype.slice.call(arguments)));
            };

        fNOP.prototype = this.prototype;
        fBound.prototype = new fNOP();

        return fBound;
    };
}

if (typeof console === 'undefined') {
    window._x = function () {};
    window.console = {
        log: x,
        warn: x,
        error: x,
        dir: x
    };
}

let amod = (function (registerApp, global, storage) {

    /**
     * Карирует функцию
     * @param {function} fn
     */
    function curry (fn) {
        function curryCheck (args) {
            if (args.length >= fn.length) {
                return fn.apply(null, args);
            }
            return function curryCaller () {
                let newArgs = [].slice.apply(arguments);
                return curryCheck(args.concat(newArgs));
            };
        }
        return curryCheck([]);
    }

    /**
     * Регистрирует компонент
     * @param {{}} storage
     * @param {string} id
     * @param {*} component
     */
    function registerComponent (storage, id, component) {
        if (typeof id !== "string") throw new TypeError(`Parameter "id" must be a string, ${typeof id} given`);
        id = id.trim().toLowerCase();
        let parent = storage.components;
        if (component.construct) parent = storage.modules;
        if (parent[id] !== undefined) throw new Error(`Component with id "${id}" already registered`);
        parent[id] = component;
    }

    /**
     * Регистрирует мастер-модуль
     * @param {{}} storage
     * @param {*} master
     */
    function registerMaster(storage, master) {
        storage.master = master;
    }

    /**
     * Возвращает компонент
     * @param {{}} storage
     * @param {string} id
     * @returns {*}
     */
    function getComponent (storage, id) {
        id = id.trim().toLowerCase();
        return storage.components[id];
    }

    /**
     * Добавляет наблюдателя за событием
     * @param {{}} listeners Хранилище слушателей для каналов
     * @param {string} channel Прослушиваемый канал
     * @param {function} listener Функция, которой будет передана информация
     */
    function listen (listeners, channel, listener) {
        if (!listeners[channel]) {
            listeners[channel] = [];
        }
        listeners[channel].push(listener);
    }

    /**
     * Оповещает наблюдателей о события
     * @param {{}} listeners Хранилище слушателей для каналов
     * @param {string} channel Канал, в который будет передаваться информация
     * @param {*} [data] Один или несколько аргументов.ю которые будут переданы слушателям
     * @returns {boolean} Возвращет false если на канале нет ни одного слушателя, иначе true
     */
    function say (listeners, channel, ...data) {
        if (!listeners[channel] || listeners[channel].length === 0) return false;
        for (let i=0; i<listeners[channel].length; i++) {
            try {
                listeners[channel][i].apply(null, data);
            } catch (e) {
                setTimeout(()=>{throw e}, 0);
            }
        }
        return true;
    }

    /*
     * Запуск amod
     */
    registerApp(
        curry(registerComponent)(storage),
        curry(registerMaster)(storage),
        curry(getComponent)(storage),
        curry(listen)(storage.listeners),
        curry(say)(storage.listeners),
        curry
    );

    storage.master.construct(global, storage.modules);

    return global;

})(
    // регистрация компонентов
    function registerApp (component, master, use, listen, say, curry) {
        //---
    },

    // глобальный объект
    {},

    // хранилище модулей
    {components: {}, modules: {}, master: null, listeners: {}}
);