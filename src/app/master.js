master({

    /**
     * Конструктор мастера
     * @param {{}} global Глобальный объект amod
     * @param {{}} modules Массив с модулями
     */
    construct (global, modules) {
        if (!this.browserIsCompatibility()) {
            alert(
                '@mod может не работать в этом браузере, так как некоторые возможности JavaScript в нем не ' +
                'поддерживаются. Пожалуйста, обновите ваш браузер.'
            );
        }

        // сперва запускается захват событий с канала error
        let errorStack = [];
        this.watchErrorHandler(global, errorStack);
        this.listenErrors(global, errorStack);

        // затем подгружаются нужные компоненты
        let config = use('config');

        // запускаются модули
        this.constructModules(modules, {
            config
        });
    },

    /**
     * Проверяет совместимость браузера
     * @returns {boolean}
     */
    browserIsCompatibility () {
        return !(
            Object.defineProperty === undefined
        );
    },

    /**
     * Производи запуск модулей (модулями считаются компоненты, у которых есть функция construct)
     * @param {{}} modules Массив с модулями
     * @param {{}} api Функционал для модулей
     */
    constructModules (modules, api) {
        for (let module in modules) {
            if (!modules.hasOwnProperty(module)) continue;
            try {
                modules[module].construct(api);
            } catch (e) {
                say('error', `Cannot run module "${module}": ${e}`);
            }
        }
    },

    /**
     * Слушает канал ошибок и передает их пользовательскому обработчику
     * @param {{}} global Глобальный объект amod
     * @param {[]} stack Стек ошибок
     */
    listenErrors (global, stack) {
        listen('error', function (error) {
            if (!error) error = "UnknownError";
            console.error(error);
            let message = error;
            if (error instanceof Error) message = error.message;
            stack.push(error);
            this.pushErrors(global, stack);
        }.bind(this));
    },

    /**
     * Следит за изменением пользовательского обработчика ошибок
     * @param {{}} global Глобальный объект amod
     * @param {[]} stack Стек ошибок
     * @returns {*}
     */
    watchErrorHandler (global, stack) {
        let userHandler = null;
        let push = () => this.pushErrors(global, stack);
        Object.defineProperty(global, 'onerror', {
            set (value) {
                if (typeof value === "function") {
                    userHandler = value;
                    push();
                }
                else userHandler = null;
            },
            get: () => userHandler
        });
    },

    /**
     * Отправляет ошибки из стека в пользовательский обработчик ошибок
     * @param {{}} global Глобальный объект amod
     * @param {[]} stack Стек ошибок
     */
    pushErrors (global, stack) {
        if (!global.onerror) return;
        let message;
        while (message = stack.shift()) {
            try {
                global.onerror(message);
            } catch (e) {
                setTimeout(()=>{throw e}, 0);
            }
        }
    }

});