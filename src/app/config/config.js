component('config', {
    userAvatar: "http://amod.lark.ru/avatar_empty.png",
    guestAvatar: "http://amod.lark.ru/avatar_guest.png",
    noPhotoImg: "",
    online: null,
    htmlOnline: "online",
    htmlOffline: "offline",
    autoinject: true,
    hideUnknown: false,
    cacheExpires: 1000 * 60 * 5 // 5min
});