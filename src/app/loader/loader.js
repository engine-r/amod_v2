component('loader', function (config) {

    let cache = {};
    let queue = {};
    let fn = use('loader/functions');

    return {
        get: curry(fn.get.bind(fn))(cache, queue)
    }

});