component('loader/functions', {

    get (cache, queue, url, callback) {
        url = url.trim().toLowerCase();
        if (cache[url]) {
            if (cache[url].expires < (new Date).getTime()) {
                delete cache[url];
            } else {
                callback(cache[url].data);
                return;
            }
        }
        if (!queue[url]) queue[url] = [];
        queue[url].push(callback);
        if (queue[url].length > 1) return;
        this._load(cache, queue, url)
    },

    _load (cache, queue, url, data=null) {
        let http = new XMLHttpRequest();
        http.open(data ? 'post' : 'get', url);
        http.onload = () => this._clearQueue(cache, queue, url, http.responseText);
        http.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        http.send(data);
    },

    _clearQueue (cache, queue, url, data) {
        cache[url] = {
            expires: (new Date()).getTime() + use('config').cacheExpires,
            data
        };
        for (let i=0; i<queue[url].length; i++) {
            try {
                queue[url][i](data)
            } catch (e) {
                say('error', e);
            }
        }
        delete queue[url];
    }

});