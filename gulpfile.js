"use strict";

const _package = require('./package.json');
const headerFile = './src/header.txt';

const fs = require('fs');
const del = require('del');
const gulp = require('gulp');
const babel = require('gulp-babel');
const concat = require('gulp-concat');
const header = require('gulp-header');
const uglify = require('gulp-uglify');
const sourcemaps = require('gulp-sourcemaps');

let headerText = '';
let src = [
    '!src/boot.js',
    'src/boot.js.part1',
    'src/app/**/*.js',
    'src/boot.js.part2'
];

/**
 * Подготавливает шапку для файла сборки
 */
gulp.task('read-header', function (done) {
    if (!fs.existsSync(headerFile)) {
        headerText = '';
        done();
        return;
    }

    let x;
    let d = new Date();
    let day = ((x = d.getUTCDate()) < 10) ? '0'+x : x;
    let month = ((x = d.getUTCMonth()) < 10) ? '0'+x : x;
    let year = d.getUTCFullYear();
    let hours = ((x = d.getUTCHours()) < 10) ? '0'+x : x;
    let minutes = ((x = d.getUTCMinutes()) < 10) ? '0'+x : x;
    let seconds = ((x = d.getUTCSeconds()) < 10) ? '0'+x : x;

    let date = `${day}.${month}.${year} ${hours}:${minutes}:${seconds} (dd.mm.yyyy h:m:s) UTC`;
    headerText = fs.readFileSync(headerFile, 'utf-8').trim() + '\n\n';
    headerText = headerText
        .replace(/<%=\s*version\s*%>/g, _package.version)
        .replace(/<%=\s*date\s*%>/g, date);
    done();
});

/**
 * Разбивает boot.js на части для последующей сборки
 */
gulp.task('build-boot', function (done) {
    let boot = fs.readFileSync('./src/boot.js', {encoding: 'utf8'});
    boot = boot.split(/\/\/\s*-{3,}.*\r?\n/);
    for (let i=0; i<boot.length; i++) {
        let file = `./src/boot.js.part${i+1}`;
        if (fs.existsSync(file)) {
            fs.unlinkSync(file);
        }
        fs.writeFileSync(file, boot[i]);
    }
    done();
});

/**
 * Очищает директорию сборки перед новой сборкой
 */
gulp.task('clear', function () {
    return del('build');
});

/**
 * Выполняет отладочную сборку, включающую sourcemaps
 */
gulp.task('build-dev', function () {
    return gulp.src(src)
        .pipe(sourcemaps.init())
        .pipe(concat('build.js'))
        .pipe(babel({
            presets: ['es2015']
        }))
        .pipe(header(headerText))
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest('build'));
});

/**
 * Выполняет минифицированную сборку
 */
gulp.task('build-min', function () {
    return gulp.src(src)
        .pipe(concat('build.min.js'))
        .pipe(babel({
            presets: ['es2015']
        }))
        .pipe(uglify())
        .pipe(header(headerText))
        .pipe(gulp.dest('build'));
});

/**
 * Наблюдает за файлами проекта, и при их изменении перезапскает сборку
 */
gulp.task('watch-src', function () {
    return gulp.watch(['src/**/*.js', headerFile], gulp.series(
        'clear',
        'read-header',
        'build-boot',
        'build-dev',
        'build-min'
    ));
});

/**
 * Задача по сборке по умолчанию
 */
gulp.task('default', gulp.series(
    'clear',
    'read-header',
    'build-boot',
    'build-dev',
    'build-min',
    'watch-src'
));
